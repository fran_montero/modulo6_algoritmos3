const TIPODINERO = Array(500, 200, 100, 50, 20, 10, 5, 2, 1, 0.5, 0.20, 0.10, 0.05, 0.02, 0.01);
const CAMBIO = Array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);


let inputImporteTotal = document.getElementById('importeTotal');
let inputImporteEntregado = document.getElementById('importeEntregado');
let resultadoCambio = document.getElementById('resultadoCambio');

function calcularCambio() {
    let importeTotal = parseFloat(inputImporteTotal.value).toFixed(2);
    let importeEntregado = parseFloat(inputImporteEntregado.value).toFixed(2);

    let aDevolver = importeEntregado - importeTotal;
        resultadoCambio.innerHTML = "<b>Cambio:</b>";
        for(var i=0; i < TIPODINERO.length; i++) {
            if( aDevolver >= TIPODINERO[i]) {

                // Calculando número de monedas para este tipo
                CAMBIO[i] = parseInt (aDevolver / TIPODINERO[i]);

                // Restamos el valor a la cantidad a devolver
                aDevolver = (aDevolver - (CAMBIO[i] * TIPODINERO[i])).toFixed(2);

                // Mostrando resultado
                let tipoMoneda = "billete";
                if (i >= 5) {
                    tipoMoneda = "moneda";
                }
                if (CAMBIO[i] > 1) {
                    tipoMoneda += "s";
                }

                const li = document.createElement("li");
                li.innerHTML = CAMBIO[i] + " " + tipoMoneda + " de " + TIPODINERO[i] + "€";
                resultadoCambio.appendChild(li);
            }
        }

    
}