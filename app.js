// Constantes
var WORK_HOURS = [
    "08:00 - 09:00",
    "09:00 - 10:00",
    "10:00 - 11:00",
    "11:00 - 12:00",
    "12:00 - 13:00",
    "13:00 - 14:00",
    "15:00 - 16:00",
    "16:00 - 17:00"
];

// Datos
var myTeam = [
    {
        name: "María",
        availability: new Array(8).fill(true)
    },
    {
        name: "Pedro",
        availability: new Array(8).fill(true)
    },
    {
        name: "Esther",
        availability: new Array(8).fill(true)
    },
    {
        name: "Marcos",
        availability: new Array(8).fill(true)
    },
];



for (let i = 0; i < myTeam.length; i++) {
    console.log("Disponibilidad de " + myTeam[i]['name'] + ":");
    for (let j = 0; j < myTeam[i]['availability'].length; j++) {
        // Generamos aleatoriamente la disponibilidad de caa miembro del equipo
        myTeam[i]['availability'][j] = Math.random() >=0.5; // 0.5 indica un 50% de porcentaje de trues. Si se baja, aumenta los trues y viceversa

        // Mostramos la agenda por consola
        let disponible = "";        
        disponible = myTeam[i]['availability'][j] ? "Sí" : "No";
        console.log(WORK_HOURS[j] + ": " + disponible);
    }
}
console.log("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

// Comprobamos la primera hora que todos tengan libre
let huecoEncontrado = false;
let horarioHueco = 0;
for (let i = 0; i < myTeam.length; i++) {    
    for (let j = 0; j < myTeam[i]['availability'].length; j++) {
        if (i==0 && !huecoEncontrado) {
            let iPosition = 0;
            let arrayHora = [];
            let encontrado = true;
            while (iPosition < myTeam.length) {
                arrayHora.push(myTeam[iPosition]['availability'][j]);
                //Si alguno es false, quiere decir que en este rango no hay posibilidad de hueco
                if(!myTeam[iPosition]['availability'][j]) {
                    encontrado = false;
                }
                iPosition++;            
            }

            // Si se ha encontrado algún hueco, paramos la comprobación
            if(encontrado) {
                huecoEncontrado = true;
                horarioHueco = j;
            } else {
                encontrado = true;
            }
           
        }
    }
}

if (!huecoEncontrado) {
    console.log ("Lo siento. No hay hueco disponible en el equipo.");
} else {
    console.log ("Hueco encontrado en el horario " + WORK_HOURS[horarioHueco]);
}



